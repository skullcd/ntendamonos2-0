<?php
    require("conex.php");
    $con = conexion();
    $usuario = $_GET["usuario"];
    $array = [];
    $qry = "SELECT * FROM verificar_palabras WHERE estatus = 'rechazada' AND users_id != {$usuario} AND users_idApro != {$usuario} GROUP BY id_palabra1";
    $res = $con->query($qry);
    while($datos = $res->fetch_row()){
        $qryS = "SELECT * FROM palabras WHERE id = {$datos[2]}";
        $resS = $con->query($qryS);
        while($datosS = $resS->fetch_row()){
                $qryP = "SELECT iconoPais, id FROM pais WHERE id = {$datosS[2]}";
                $resP = $con->query($qryP);
                while($datosP = $resP->fetch_row()){
                    $array[] = [$datosS[0], $datosS[1], $datosP[0], $datosP[1], $datosS[2]];
                }
        }
    }
    echo json_encode($array, JSON_UNESCAPED_UNICODE);
?>
