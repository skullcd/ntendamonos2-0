<?php
    require("conex.php");
    $con = conexion();
    $id = $_GET["id"];
    $usuario = $_GET["usuario"];
    $qry = "SELECT * FROM verificar_palabras WHERE id = {$id}";
    $res = $con->query($qry);
    while($datos = $res->fetch_row()){
        $puntos = $datos[6] + 1;
        $aux = "aprobada";
        if($puntos >= 10){
            disminuirPuntos($datos[1], $con);
            disminuirPuntos($datos[5], $con);
            $aux = "rechazada";
        }
        $qryUp = "UPDATE verificar_palabras SET reportada = {$puntos}, estatus = '{$aux}' WHERE id = {$id}";
        if($con->query($qryUp)){
            echo "correcto";
        }
    }

    /**
    * Function disminuirPuntos
    * Funcion que disminuye el puntaje de los usuarios
    * Si el puntaje es menor a 0 pasa a ser usuario normal
    * @param $idUsuario
    */
    function disminuirPuntos($idUsuario, $con){
        $qryUs = "SELECT * FROM users WHERE id = {$idUsuario}";
        $resUs = $con->query($qryUs);
        while($datosUs = $resUs->fetch_row()){
            $puntosUs = $datosUs[8]-5;
            $auxUs = "";
            if($puntosUs < 0){
                $auxUs = "normal";
            }else{
                $auxUs = "moderador";
            }
            $qryUsUp = "UPDATE users SET tipo = '{$auxUs}', puntos = {$puntosUs} WHERE id = {$idUsuario}";
            $con->query($qryUsUp);
        }
    }
?>
