<?php
    require("conex.php");
    $con = conexion();
    $sugerida = $_GET["palabraSugerida"];
    $paisSugerida = $_GET["paisSugerida"];
    $definicion1 = $_GET["definicion1"];
    $definicion_unesco = $_GET["definicion_unesco"];
    $definicion_ifilac = $_GET["definicion_ifilac"];
    $palabraRelacion = $_GET["palabraRelacion"];
    $paisRelacion = $_GET["paisRelacion"];

    if ($paisRelacion == "costarica") {
     $paisRelacion = "costa_rica";
   }
    $id_usuario = $_GET['usuario'];
    $query = "SELECT id FROM pais WHERE nombrePais LIKE '{$paisRelacion}'";
    $id_relacion = $con->query($query)->fetch_array();
    if($sugerida == $palabraRelacion && $paisSugerida == $id_relacion[0]){
        echo "Error";
      }else{
        $id_palabraRelacion = $id_relacion[0];
        $idPalabra1 = "";
        $idPalabraR = "";
        if ($palabraRelacion == "") {
            $idPalabra1 = operacionPalabra($sugerida,$paisSugerida, $con);
            $res = "Exitoso";
        } else {
          $idPalabra1 = operacionPalabra($sugerida,$paisSugerida, $con);
          $idPalabraR = operacionPalabra($palabraRelacion,$id_palabraRelacion, $con);
          $res = relacionarPalabras($idPalabra1,$idPalabraR, $con, $id_usuario, $definicion1, $definicion_ifilac, $definicion_unesco);
        }
        if($res == "Exitoso"){
          echo "Exitoso";
        }else{
          echo "Duplicado";
        }
      }

/**
     *  function operacionPalabra
     *  Insertar palabras en Palabra
     *  @param $palabra
     *  @param $idPais
     *  @return $idPalabra1
     */
    function operacionPalabra($palabra, $idPais, $con){
        $idPalabra1= "";
        $qry = "SELECT id FROM palabras WHERE palabra LIKE '{$palabra}' AND id_pais = {$idPais}";
        $res = $con->query($qry);
        if($res->num_rows > 0){
            $res = $res->fetch_array();
            $idPalabra1 = $res[0];
        }else{
            $date = date('Y-m-d H:i:s');
            $qry2 = "INSERT INTO palabras (palabra, id_pais, estatus, created_at, updated_at) VALUES ('{$palabra}', {$idPais}, 'incorrecta', '{$date}', '{$date}')";
            if($con->query($qry2)){
                $qry3 = "SELECT id FROM palabras WHERE palabra LIKE '{$palabra}' AND id_pais = {$idPais}";
                $res3 = $con->query($qry3);
                $res3 = $res3->fetch_row();
                $idPalabra1 = $res3[0];
            }
        }
        return $idPalabra1;
    }
    /**
     *  function relacionarPalabras
     *  Insertar id palabras en tabla relacions
     *  @param $idPalabra1
     *  @param $idPalabraR
     *  @return string
     */
     function relacionarPalabras($idPalabra1, $idPalabraR, $con, $id_usuario, $definicion1, $definicion_ifilac, $definicion_unesco){
        //  $qry = "SELECT * FROM verificar_palabras WHERE id_palabra1 = {$idPalabra1} AND id_palabra2 = {$idPalabraR}";
        //  $res = $con->query($qry);
        //  if($res->num_rows > 0){
        //      return "Repetidos";
        //  }else{
           if ($definicion1 == "ninguna def") {
             $qry = "SELECT Definicion, glosario_ifilac, Glosario_unesco FROM verificar_palabras WHERE id_palabra1 = {$idPalabra1}";
             $res = $con->query($qry)->fetch_array();
             $definicion1 = $res[0];
             $definicion_ifilac = $res[1];
             $definicion_unesco = $res[2];
           }
             $qry = "SELECT * FROM verificar_palabras WHERE id_palabra1 = {$idPalabra1} AND id_palabra2 = {$idPalabraR}";
             $res = $con->query($qry);
             if($res->num_rows > 0){
                 return "Repetidos";
             }else{
                 $date = date('Y-m-d H:i:s');
                 $qry6  = "INSERT INTO verificar_palabras(users_id, users_idApro, id_palabra1, id_palabra2, created_at, updated_at, Definicion, glosario_ifilac, Glosario_unesco) VALUES({$id_usuario}, 1, {$idPalabra1}, {$idPalabraR}, '{$date}', '{$date}', '{$definicion1}', '{$definicion_ifilac}', '{$definicion_unesco}')";
                 if($con->query($qry6)){
                     return "Exitoso";
                 }
             }
        //  }
     }
 ?>
