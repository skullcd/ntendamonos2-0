<?php
    require("conex.php");
    $con = conexion();
    $palabra = $_GET["id"];
    $usuario = $_GET["usuario"];
    $qry = "UPDATE verificar_palabras SET estatus = 'aprobada', users_idApro = {$usuario} WHERE id = {$palabra}";
    if($con->query($qry)){
        $qry2 = "SELECT * FROM verificar_palabras WHERE id = {$palabra}";
        $res = $con->query($qry2);
        while($datos = $res->fetch_row()){
            actualizarPuntos($datos[1], $con);
            $id_pais1 = actualizarPalabras($datos[2], $con);
            $id_pais2 = actualizarPalabras($datos[3], $con);
            $nombre_pais1 = nombrePais($id_pais1, $con);
            $nombre_pais2 = nombrePais($id_pais2, $con);
            insertarRelacion($datos[2], $datos[3], $nombre_pais1, $nombre_pais2 , $con);
        }
        echo "correcto";
    }else{
        echo "Error";
    }
    function actualizarPuntos($usuario, $con){
        $qry = "SELECT puntos FROM users WHERE id = {$usuario}";
        $res2 = $con->query($qry);
        while($datos = $res2->fetch_row()){
            $points = 0;
            if($datos[0] < 0){
             $points = $datos[0] + 5;
            }else{
                $points = $datos[0];
            }
            $aux_user = "";
            if($points >= 0){
                $aux_user = "moderador";
            }else{
                $aux_user = "normal";
            }
            $qryUp = "UPDATE users SET tipo = '{$aux_user}', puntos = {$points} WHERE id = {$usuario}";
            $res = $con->query($qryUp);
        }
    }
    function actualizarPalabras($palabra, $con){
        $qry = "UPDATE palabras SET estatus = 'correcta' WHERE id = {$palabra}";
        $res = $con->query($qry);

        $qry2 = "SELECT id_pais FROM palabras WHERE id = {$palabra}";
        $res2 = $con->query($qry2)->fetch_array();
        return $res2[0];
    }

    function nombrePais($id_pais, $con){
      $qry2 = "SELECT nombrePais FROM pais WHERE id = {$id_pais}";
      $res2 = $con->query($qry2)->fetch_array();
      if ($res2[0] == "México") {
        $res2[0] = "mexico";
      } elseif ($res2[0] == "Perú" ) {
        $res2[0] =  "peru";
      } elseif ($res2[0] == "Panamá") {
        $res2[0] = "panama";
      } elseif ($res2[0] == "Costa Rica") {
        $res2[0] = "costa_rica";
      }
      return $res2[0];
    }

    function insertarRelacion($idPalabra1, $idPalabra2, $paisSugerido, $paisRelacion, $con){

      $qry = "SELECT Definicion, glosario_ifilac, Glosario_unesco FROM verificar_palabras WHERE id_palabra1 = {$idPalabra1}";
      $res = $con->query($qry)->fetch_array();
      $definicion1 = $res[0];
      $definicion_ifilac = $res[1];
      $definicion_unesco = $res[2];


      $qry4 = "SELECT * FROM relacions WHERE palabra_$paisSugerido = {$idPalabra1}";
      $res4 = $con->query($qry4);
      if($res4->num_rows > 0){
        $qry6 = "UPDATE relacions SET palabra_$paisRelacion = {$idPalabra2} , palabra_$paisSugerido = {$idPalabra1} WHERE palabra_$paisSugerido = {$idPalabra1}";
        if($con->query($qry6)){
            return "Exitoso";
        }
      } else{
            $qry3 ="INSERT INTO relacions (palabra_$paisSugerido, palabra_$paisRelacion, Definicion, glosario_ifilac, Glosario_unesco) VALUES({$idPalabra1}, {$idPalabra2}, '{$definicion1}', '{$definicion_ifilac}', '{$definicion_unesco}')";
            $res3 = $con->query($qry3);
        }
    }

?>
