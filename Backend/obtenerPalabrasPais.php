<?php 
    require("conex.php");
    $con = conexion();
    $pais = $_GET["pais"];
    $qry = "SELECT id, palabra FROM palabras WHERE id_pais = {$pais} AND estatus = 'correcta'";
    $res = $con->query($qry);
    $data = [];
    while($row = $res->fetch_array()){
        $data[] = $row;
    }
    echo json_encode($data, JSON_UNESCAPED_UNICODE);
?>