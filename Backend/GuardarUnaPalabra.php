<?php
    require("conex.php");
    $con = conexion();
    $sugerida = $_GET["palabraSugerida"];
    $paisSugerida = $_GET["paisSugerida"];
    $definicion1 = $_GET["definicion1"];
    $definicion_unesco = $_GET["definicion_unesco"];
    $definicion_ifilac = $_GET["definicion_ifilac"];

    $query = "SELECT nombrePais FROM pais WHERE id = {$paisSugerida}";
    $id_relacion = $con->query($query)->fetch_array();

    $pais_texto = $id_relacion[0];
    $idPalabra1 = "";
    $idPalabraR = "";
    $idPalabra1 = operacionPalabra($sugerida,$paisSugerida, $con);
    $res = relacionarPalabras($idPalabra1, $pais_texto, $con, $definicion1, $definicion_ifilac, $definicion_unesco);
    if($res == "Exitoso"){
      echo "Exitoso";
    }else{
      echo "Duplicado";
    }

/**
     *  function operacionPalabra
     *  Insertar palabras en Palabra
     *  @param $palabra
     *  @param $idPais
     *  @return $idPalabra1
     */
    function operacionPalabra($palabra, $idPais, $con){
        $idPalabra1= "";
        $qry = "SELECT id FROM palabras WHERE palabra LIKE '{$palabra}' AND id_pais = {$idPais}";
        $res = $con->query($qry);
        if($res->num_rows > 0){
            $res = $res->fetch_array();
            $idPalabra1 = $res[0];
        }else{
            $date = date('Y-m-d H:i:s');
            $qry2 = "INSERT INTO palabras (palabra, id_pais, estatus, created_at, updated_at) VALUES ('{$palabra}', {$idPais}, 'correcta', '{$date}', '{$date}')";
            if($con->query($qry2)){
                $qry3 = "SELECT id FROM palabras WHERE palabra LIKE '{$palabra}' AND id_pais = {$idPais}";
                $res3 = $con->query($qry3);
                $res3 = $res3->fetch_row();
                $idPalabra1 = $res3[0];
            }
        }
        return $idPalabra1;
    }
    /**
     *  function relacionarPalabras
     *  Insertar id palabras en tabla relacions
     *  @param $idPalabra1
     *  @param $idPalabraR
     *  @return string
     */
     function relacionarPalabras($idPalabra1, $pais_texto, $con, $definicion1, $definicion_ifilac, $definicion_unesco){
         $date = date('Y-m-d H:i:s');
         $qry6  = "INSERT INTO relacions(palabra_$pais_texto, Definicion, glosario_ifilac, Glosario_unesco) VALUES($idPalabra1, '{$definicion1}', '{$definicion_ifilac}', '{$definicion_unesco}')";
         if($con->query($qry6)){
             return "Exitoso";
         }
        //  }
     }
 ?>
