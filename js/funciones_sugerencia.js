/**
*   Function Menu
*   Muestra las opciones adecuadas dependiendo del tipo
*   de usuario
*/
$(function(){
    var cont = "";
    if(localStorage.getItem('id_usuario')){
        user = localStorage.getItem('id_usuario');
        usuario = localStorage.getItem('usuario');
        fotoPerfil = localStorage.getItem('fotoPerfil');
        tipoUsuario = localStorage.getItem('tipoUsuario');
        $("#textoBienvenida").html("¿Qué puedes hacer "+usuario+" ?");
        $("#tituloMenu").html("<h1>"+usuario+"</h1>");
        $("#imgPerM").attr("src", "img/imgPerfil/"+fotoPerfil);
        cont = "<a href='perfil.html'><div class='opciones'><img src='img/profile.png' alt='icono' /><h2>Perfil</h2></div></a>"+
            "<a href='sugerirPalabra.html'><div class=' opciones'><img src='img/sugerir.png' alt='icono' /><h2>Sugerir Relación</h2></div></a>";
        if(tipoUsuario == "moderador"){
           cont += "<a href='agregarUnapalabra.html'><div class='opciones'><img src='img/add.png' alt='icono'/><h2>Agregar Palabra</h2></div></a>"+
           "<a href='aceptarSugerencia.html'><div class='opciones'><img src='img/sugeridas.png' alt='icono'/><h2>Sugerencias</h2></div></a>"+
        "<a href='aceptarReevaluacion.html'><div class='opciones'><img src='img/research.png' alt='icono' /><h2>Reevaluación</h2></div></a>";
           }
    }else{
        $("#textoBienvenida").html("¿Qué puedes hacer ?");
        $("#tituloMenu").html("");
        $("#imgPerM").attr("src", "img/imgPerfil/default.jpg");
        cont = "";
    }
    $("#cont_menu").html(cont);
});

/**
* Function cerrarSesion
* Accese al metodo cerrarSesion para eliminar variables de sesion en php
* Si manda un correcto elimina el localStorage
*/
$(document).ready(function(){
  $("#salirSesion").click(function(event) {
      event.preventDefault();
      localStorage.removeItem('id_usuario');
        localStorage.removeItem('usuario');
        localStorage.removeItem('fotoPerfil');
        localStorage.removeItem('tipoUsuario');
        window.location = 'index.html';
  });
});

/**
* function desplegarMenu
* Muestra el menu al momento de dar click
*/
function desplegarMenu(){
  var menu2 = document.getElementById("menu2");
  var menu1 = document.getElementById("menu1");
  menu2.classList.add('desplegarMenuM2');
  menu1.classList.add('desplegarMenuM1');
}

/**
* function ocultarMenu
* Oculta el menu al momento de dar click
*/
function ocultarMenu(){
  var menu2 = document.getElementById("menu2");
  var menu1 = document.getElementById("menu1");
  menu2.classList.remove('desplegarMenuM2');
  menu1.classList.remove('desplegarMenuM1');
}
//________________________Sección de sugerencias
/**
* Function onchange Pais
*/
$(document).ready(function(){
  if($("#paisRegistro").length || $("#paisRegistro3").length){
      var con = "";
      var todos = "<option value='0'>Todos</option>";
    $.ajax({
      url: 'Backend/obtenerPaises.php',
      type:'GET',
      success: function(data){
          data = JSON.parse(data);
          for(pais in data){
              con = con + "<option value='"+data[pais][0]+"'>"+data[pais][1]+"</option>";
          }
          if ($("#paisRegistro").length) {
            $("#paisRegistro").html(con);
            $("#paisRegistro2").html(con);
            self.cargarPalabras("todos");
          } else if ($("#paisRegistro3").length) {
              con = todos + con;
              $("#paisRegistro3").html(con);
          }
      }
    });
  }
});


$(document).ready(function(){
  con = "";
  $.ajax({
    url: 'Backend/buscarTodasPalabras.php',
    type:'GET',
    success: function(data){
        if(data != "ninguna"){
            data = JSON.parse(data);
            for(palabras in data){
                con += "<div onclick='resultados("+data[palabras][3]+")' class=' col-sm-5 col-md-5 col-lg-4 respuesta sugeridas'>"+
                "<div class='banderaP'><img src='img/banderas/"+data[palabras][0]+"' alt='bandera' class='banderaEspe'/></div>"+
                  "<div class='text-left pais2'><h1>"+data[palabras][1]+"</h1></div>"+
                  "<div class='text-center aceptar'><img src='img/next.png' alt='aceptar' onclick='resultados("+data[palabras][3]+")'></div></div>";
            }
        }else{
            con = "<div class='text-center emptyP empty'>"+
          "<img src='img/clear.png' alt='empty' />"+
          "<h3>Sin Resultados</h3></div>";
        }
        $("#respuestas2").html(con);
    }
    });
});


$(function(){
  $("#paisRegistro").change(function(){
    cargarPalabras("uno");
  });
  $("#paisRegistro2").change(function(){
    cargarPalabras("dos");
  });
  $("#paisRegistro3").change(function(){
    id_pais = $('#paisRegistro3').val();
    if (id_pais == 0) {
      url = 'Backend/buscarTodasPalabras.php';
    } else {
      url =  'Backend/buscarPalabras.php';
    }
    con = "";
    $.ajax({
      url: url,
      type:'GET',
      data:{id_pais:id_pais},
      success: function(data){
          if(data != "ninguna"){
              data = JSON.parse(data);
              for(palabras in data){
                  con += "<div onclick='resultados("+data[palabras][3]+")' class=' col-sm-5 col-md-5 col-lg-4 respuesta sugeridas'>"+
                "<div class='banderaP'><img src='img/banderas/"+data[palabras][0]+"' alt='bandera' class='banderaEspe'/></div>"+
                  "<div class='text-left pais2'><h1>"+data[palabras][1]+"</h1></div>"+
                  "<div class='text-center aceptar'><img src='img/next.png' alt='aceptar' onclick='resultados("+data[palabras][3]+")'></div></div>";
              }
          }else{
              con = "<div class='text-center emptyP empty'>"+
            "<img src='img/clear.png' alt='empty' />"+
            "<h3>Sin Resultados</h3></div>";
          }
          $("#respuestas2").html(con);
      }
      });
  });
});
/**
* Function cargarPalabras
* Obtiene las palabras disponibles
*/
function cargarPalabras(tipo){
    aux = "";
    if(tipo == "todos" || tipo == "dos"){
        aux = $('#paisRegistro').val();
       }
    if(tipo == "uno"){
        aux = $("#paisRegistro").val();
       }
  if(aux !=  ""){
    var idPais = aux;
    $.ajax({
      url: 'Backend/obtenerPalabrasPais.php',
      type:'GET',
      data: {pais:idPais},
      success: function(data){
        if(data != ""){
            data = JSON.parse(data);
            var con = "";
            for(palabras in data){
              con = con + "<option data-value='"+data[palabras][0]+"'>"+data[palabras][1]+"</option>"
            }
            if(tipo == "todos"){
                $("#relacionadaPalabra").html(con);
                $("#relacionadaPalabra2").html(con);
            }
            if(tipo == "dos"){
               $("#relacionadaPalabra2").html(con);
            }
            if(tipo == "uno"){
               $("#relacionadaPalabra").html(con);
            }

         }
      }
    });
  }
}

/**
* Function verificarDescripcion
* Muestra o no el input para ingresar la definicion de la palabra
* dependiendo de la respuesta del ajax
* @param palabra
* @param pais
* @param tipo
*/
function verificarDescripcion(palabra, pais, tipo){
  contenido_sin_relacion= "";
  contenido_con_relacion= "";
  contenido="";
  sin_relacion = [];
  relacion = [];
  var tipoDef = "";
  if(tipo=="palabra2"){
    tipoDef = "#definicion2"
  }else{
    tipoDef = "#definicion1"
  }
  pais_texto = $("#paisRegistro option[value='"+pais+"']").text();
  $.ajax({
    url: 'Backend/obtenerDescripcionPalabra.php',
    type:'GET',
    data: {palabra:palabra, pais:pais, pais_texto:pais_texto},
    success: function(data){
      if (data != "") {
          data = JSON.parse(data);
      }
          pais2 ="";
          $("#definicion_unesco").css("display","none");
          $("#definicion_ifilac").css("display","none");
          $(tipoDef).css("display","none");
          for (var i = 0; i < data.length; i++) {
            if (data[i].split(':')[1]== "no") {
              sin_relacion.push(data[i].split(':')[0]);
              pais2 = data[i].split(":")[0];
              pais2 = pais2.split('.')[0];
              contenido_sin_relacion = "<div class='pais_relacionado col-lg-3 col-md-5 col-sm-2 col-xs-3'><img onclick=\"relacionar_palabra(\'"+pais2+"\', \'"+palabra+"\')\"' src='img/banderas/"+data[i].split(':')[0]+"' id='id_"+pais2+"' alt=''></div>"+contenido_sin_relacion;
            } else {
              contenido_con_relacion = "<div class='pais_relacionado background_white col-lg-3 col-md-5 col-sm-2 col-xs-3'><img  src='img/banderas/"+data[i].split(':')[0]+"' alt=''></div>"+contenido_con_relacion;
              relacion.push(data[i].split(':')[0]);

            }
          }

          /*if (relacion.length == 4) {
              $(".paises_con_relacion").css("height","150px");
          } else if (relacion.length == 5 || relacion.length == 6 ) {
            $(".paises_con_relacion").css("height","170px");
          }else if (relacion.length == 7  || relacion.length == 8 || relacion.length == 9) {
            $(".paises_con_relacion").css("height","210px");
          } else if (relacion.length == 10  || relacion.length == 11 || relacion.length == 12) {
            $(".paises_con_relacion").css("height","270px");
          } else if (relacion.length == 13  || relacion.length == 14 || relacion.length == 15) {
            $(".paises_con_relacion").css("height","340px");
          }*/

          if (relacion.length == data.length) {
              $(".paises_sin_relacion").css("display","none");
              $(".con_relacion").html(contenido_con_relacion);
              $(".paises_con_relacion").css("display","block");
              $(".espacio-espera").css("display","none");
          } else if (sin_relacion.length == data.length) {
            $(".paises_con_relacion").css("display","none");
            $(".sin_relacion").html(contenido_sin_relacion);
            $(".paises_sin_relacion").css("display","block")
            $(tipoDef).css("display","block");
            $("#definicion_unesco").css("display","block");
            $("#definicion_ifilac").css("display","block");
            $(".espacio-espera").css("display","none");
          } else {
            $(".con_relacion").html(contenido_con_relacion);
            $(".paises_con_relacion").css("display","block");
            $(".sin_relacion").html(contenido_sin_relacion);
            $(".paises_sin_relacion").css("display","block")
            $(".espacio-espera").css("display","none");
          }
    }
  });
}


function relacionar_palabra(pais, palabra){
  $(".pais_relacionado img").css("height","100%");
  $(".pais_relacionado img").css("width","100%");
  $("#palabra2").css("display","block");
  $("#paisRegistro2").attr("value",pais);
  $(".pais_relacionado img").css("opacity",".5");
  $("#id_"+pais).css("opacity","1");
  $("#id_"+pais).css("width","100%");
}

$(function(){
    $("#palabra").blur(function(){
       $("#palabra2").css("display","none");
       var palabra = $("#palabra").val();
        var pais = $("#paisRegistro").val();
        if(palabra != ""){
          self.verificarDescripcion(palabra, pais, "palabra1");
        } else {
          $("#definicion1").css("display","none");
          $("#definicion_ifilac").css("display","none");
          $("#definicion_unesco").css("display","none");
        }
    });
    $("#paisRegistro").blur(function(){
       var palabra = $("#palabra").val();
        var pais = $("#paisRegistro").val();
        if(palabra != ""){
          self.verificarDescripcion(palabra, pais, "palabra1");
        } else {
          $("#definicion1").css("display","none");
          $("#definicion_ifilac").css("display","none");
          $("#definicion_unesco").css("display","none");
        }
    });
});
/**
* Function focus Palabra2
* Sube el contenedor para apreciarlo mejor

$(document).ready(function(){
  $("#palabra2").focus(function(){
    $("#mover").css('top','-150px');
  });
  $("#definicion2").focus(function(){
    $("#mover").css('top','-150px');
  });
});
*/
/**
* Function blur Palabra2
* Sube el contenedor para apreciarlo mejor
*/
$(document).ready(function(){
  $("#palabra2").blur(function(){
    $("#mover").css('top','80px');
  });
  $("#definicion2").blur(function(){
    $("#mover").css('top','80px');
  });
});

/**
* Function enviarPalabras
* Recupera los datos y los inserta en la BD
*/
$(document).ready(function(){
  $("#mandarSugerencia").click(function(){
    var palabraSugerida = $("#palabra").val().toUpperCase();
    if(palabraSugerida != ""){
      var user = localStorage.getItem('id_usuario');
      var paisSugerida = $("#paisRegistro").val();
      var paisRelacion = $("#paisRegistro2").val();
      var palabraRelacion = $("#palabra2").val().toUpperCase();
      var definicion1 = "";
      if($("#definicion1").css("display")=="block"){
        if($("#definicion1").val() != "" || $("#definicion_ifilac").val() != "" || $("#definicion_unesco").val() != ""){
          definicion1 = $("#definicion1").val();
          definicion_ifilac=$("#definicion_ifilac").val();
          definicion_unesco= $("#definicion_unesco").val();
        }else{
          self.palabra("Complete Todos Los Campos");
          return;
        }
      }else{
        definicion1 = "ninguna def";
        definicion_ifilac= "ninguna def";
        definicion_unesco=  "ninguna def";
      }
      $.ajax({
        url: 'Backend/guardarPalabra.php',
        type:'GET',
        data: {palabraSugerida:palabraSugerida, paisSugerida:paisSugerida, definicion_ifilac:definicion_ifilac, definicion_unesco:definicion_unesco, definicion1:definicion1, palabraRelacion:palabraRelacion, paisRelacion: paisRelacion, usuario:user},
        success: function(data){
          if(data == "Exitoso"){
            $("#palabra").val('');
            $("#palabra2").val('');
            $("#definicion1").val('');
            $("#definicion_ifilac").val('');
            $("#definicion_unesco").val('');
            $("#definicion1").css("display","none");
            $("#definicion_ifilac").css("display","none");
            $("#definicion_unesco").css("display","none");
            $(".paises_con_relacion").css("display","none");
            $(".paises_sin_relacion").css("display","none")
            self.palabra("Gracias :D");
          }
          if(data == "Duplicado"){
            self.palabra("Relación Existente");
          }
          if(data == "Error"){
            self.palabra("No Se Puede Relacionar Asi Misma");
          }
        }
      });
    }else{
      self.palabra("Complete Todos Los Campos");
    }
  });
});




/**
* Function enviarPalabras
* Recupera los datos y los inserta en la BD
*/
$(document).ready(function(){
  $("#mandarPalabra").click(function(){
    var palabraSugerida = $("#palabra").val().toUpperCase();
    if(palabraSugerida != ""){
      var user = localStorage.getItem('id_usuario');
      var paisSugerida = $("#paisRegistro").val();
      var definicion1 = "";
      if($("#definicion1").css("display")=="block"){
        if($("#definicion1").val() != ""){
          definicion1 = $("#definicion1").val();
          definicion_ifilac=$("#definicion_ifilac").val();
          definicion_unesco= $("#definicion_unesco").val();
          $.ajax({
            url: 'Backend/guardarUnaPalabra.php',
            type:'GET',
            data: {palabraSugerida:palabraSugerida, paisSugerida:paisSugerida, definicion_ifilac:definicion_ifilac, definicion_unesco:definicion_unesco, definicion1:definicion1, usuario:user},
            success: function(data){
              if(data == "Exitoso"){
                $("#palabra").val('');
                $("#definicion1").val('');
                $("#definicion_ifilac").val('');
                $("#definicion_unesco").val('');
                $("#definicion1").css("display","none");
                $("#definicion_ifilac").css("display","none");
                $("#definicion_unesco").css("display","none");
                $(".paises_con_relacion").css("display","none");
                $(".paises_sin_relacion").css("display","none")
                self.palabra("Gracias :D");
              }
            }
          });


        }else{
          self.palabra("Complete Todos Los Campos");
          return;
        }
      }else{
        self.palabra("La Palabra Ya Esta Registrada");
      }
    }
  });
});





/**
* function esconderBusqueda
* Oculta el bloque de busqueda de Palabra
*/
function esconderBusqueda(){
  var busqueda = document.getElementById("busqueda");
  busqueda.style.display = "none";
    var aux = "";
    if(localStorage.getItem("usuario")){
        aux = localStorage.getItem("usuario");
       }
    con = '<div class="respuestas" id="respuestas">'+
          '<div class="imagenIndex"><img src="img/team.png" alt="team"></div>'+
          '<div class="titulos"><h1 id="textoBienvenida">¿Qué puedes hacer '+aux+' ?</h1>'+
            '<p>Mantenerte siempre comunicado</p><p>Estés donde estés</p></div></div>';
    $("#contenedorEspecial").html(con);
}

/**
* function esconderBusqueda
* Oculta el bloque de busqueda de Palabra
*/
function esconderBusquedaPalabras(){
  var busqueda = document.getElementById("busqueda");
  busqueda.style.display = "none";
    var aux = "";
    if(localStorage.getItem("usuario")){
        aux = localStorage.getItem("usuario");
       }
    // con = '<div class="respuestas" id="respuestas">'+
    //       '<div class="imagenIndex"><img src="img/team.png" alt="team"></div>'+
    //       '<div class="titulos"><h1 id="textoBienvenida">¿Qué puedes hacer '+aux+' ?</h1>'+
    //         '<p>Mantenerte siempre comunicado</p><p>Estés donde estés</p></div></div>';
    // $("#contenedorEspecial").html(con);
}
/**
* function mostrarBusqueda
* Muestra el cuadro de busqueda de palabras
*/
function mostrarBusqueda(){
  var busqueda = document.getElementById("busqueda");
  busqueda.style.display = "block";
}

/***************Busqueda Palabras **********/
/**
* Function busqueda
* Obtiene la palabra a buscar
* Valida si se escribio algo en el input si no muestra mensaje
* Valida si se recibe algo del controlador si no muestra imagen de vacio
* Si se encontraron resultados muestra la palabra con su pais
*/
$(document).ready(function(){
  $("#busqueda2").click(function(){
    var palabra = $("#palabraBuscar").val().toUpperCase();
      var con = "";
    if(palabra == ""){
      self.palabra("Ingrese Una Palabra");
    }else{
      $.ajax({
        url: 'Backend/buscarPalabra.php',
        type:'GET',
        data: {palabra:palabra},
        success: function(data){
            if(data != "ninguna"){
					
                data = JSON.parse(data);
                for(palabras in data){
                    con +="<div onclick='resultados("+data[palabras][0]+")' class=' col-sm-5 col-md-5 col-lg-4 respuesta sugeridas'>"+
                "<div class='banderaP'><img src='img/banderas/"+data[palabras][2]+"' alt='bandera' class='banderaEspe'/></div>"+
                  "<div class='text-left pais2'><h1>"+data[palabras][1]+"</h1></div>"+
                  "<div class='text-center aceptar'><img src='img/next.png' alt='aceptar' onclick='resultados("+data[palabras][0]+")'></div></div>";
                }
            }else{
                con = "<div class='text-center emptyP empty'>"+
              "<img src='img/clear.png' alt='empty' />"+
              "<h3>Sin Resultados</h3></div>";
            }
            $("#respuestas").html(con);
        }
      });
    }
  });
});

$(document).ready(function(){
  $("#busqueda3").click(function(){
    var palabra = $("#palabraBuscar2").val().toUpperCase();
      var con = "";
    if(palabra == ""){
      self.palabra("Ingrese Una Palabra");
    }else{
      $.ajax({
        url: 'Backend/buscarPalabra.php',
        type:'GET',
        data: {palabra:palabra},
        success: function(data){
            if(data != "ninguna"){
                data = JSON.parse(data);
                for(palabras in data){
                    con += "<div class='respuesta sugeridas'>"+
                    "<img src='img/banderas/"+data[palabras][2]+"' alt='bandera' class='banderaEspe'/>"+
                      "<div class='pais2'><h1>"+data[palabras][1]+"</h1></div>"+
                      "<div class='aceptar'><img src='img/next.png' alt='aceptar' onclick='resultados("+data[palabras][0]+")'></div></div>";
                }
            }else{
                con = "<div class='empty'>"+
              "<img src='img/clear.png' alt='empty' />"+
              "<h3>Sin Resultados</h3></div>";
            }
            $("#contenedor-principal").html(con);
        }
      });
    }
  });
});

// /**
// * Function busqueda
// * Obtiene la palabra a buscar
// * Valida si se escribio algo en el input si no muestra mensaje
// * Valida si se recibe algo del controlador si no muestra imagen de vacio
// * Si se encontraron resultados muestra la palabra con su pais
// */
// $(document).ready(function(){
//   $("#paisRegistro").change(function(){
//     var pais = document.getElementById("paisRegistro");
//       $.ajax({
//         url: 'Backend/buscarPalabras.php',
//         type:'GET',
//         data: {pais:pais},
//         success: function(data){
//             if(data != "ninguna"){
//                 data = JSON.parse(data);
//                 for(palabras in data){
//                     con += "<div class='respuesta sugeridas'>"+
//                     "<img src='img/banderas/"+data[palabras][2]+"' alt='bandera' class='banderaEspe'/>"+
//                       "<div class='pais2'><h1>"+data[palabras][1]+"</h1></div>"+
//                       "<div class='aceptar'><img src='img/next.png' alt='aceptar' onclick='resultados("+data[palabras][0]+")'></div></div>";
//                 }
//             }else{
//                 con = "<div class='empty'>"+
//               "<img src='img/clear.png' alt='empty' />"+
//               "<h3>Sin Resultados</h3></div>";
//             }
//             $("#respuestas").html(con);
//         }
//       });
//   });
// });

/**************** Resultados ***********/
/**
* Function ocultarDefinicion
* Oculta la definicion de la palabra
*/
function ocultarDefinicion(){
  $("#reporte").css('visibility','hidden');
  $("#reporte").css('opacity','0');
  $("#definicion").css('visibility','hidden');
}

/**
* Function mostrarDefinicion
* Muestra la definicion de la palabra
*/
function mostrarDefinicion(){
  $("#reporte").css('visibility','visible');
  $("#reporte").css('opacity','1');
  $("#definicion").css('visibility','visible');
}

/**
* Function ocultarReporte
* Oculta el div del reporte
*/
function ocultarReporte(){
  $("#reporte").css('visibility','hidden');
  $("#reporte").css('opacity','0');
  if($("#definicion").css('visibility')=='visible'){
    $("#definicion").css('visibility','hidden');
  }
  $("#reportein").css('visibility','hidden');
}
/**
* Function resultados
* Redirecciona a la pagina en especifico con su id
* @param id
*/
function resultados(id){
    sessionStorage.setItem("idPalabra",id);
    window.location = "resultados.html";
}

/**
* Function retrocederEspB
* De la vista de detalles de la palabra
* redirecciona al index
*/
function retrocederEspB(){
  window.location = "principal.html";
}

function obtenerDatosPalabra(){
    self.palabra("Cargando..");
    var con = "";
    idPalabra = sessionStorage.getItem("idPalabra");
    $.ajax({
        url: 'Backend/obtenerPalabrasBusqueda.php',
        type:'GET',
        data: {palabra:idPalabra},
        success: function(data){
			  console.log(data);
            data = JSON.parse(data);
            aux = "";
                    def = data[0][4];
                    def = def.split("\n");
                    for(i=0; i<def.length; i++){
                        if(def[i] == "Definición" || def[i] == "Glosario UNESCO" || def[i] == "Glosario IFILAC"){
                            aux += "<br><b>"+def[i]+"</b><br>";
                           }else{
                               aux += def[i]+" ";
                           }
                    }
            if(data.length > 1){
             for(datos in data){
                if(datos == 0){
                   $("#palabraDefinicion").html(aux);
                    $("#palabraPadreB").html(data[datos][1]);
                    $("#palabraPadreId").val(data[datos][0]);
                    $("#sTitulo").attr("onclick", "hablar('"+data[datos][1]+"',this,1)");
                }else{
                      con += "<div class='sugerenciaTarjeta' id='"+data[datos][5]+"'>";
                    con += "<img src='img/banderas/"+data[datos][2]+"' alt='bandera' class='banderaSug'/>"+
                    "<h2>"+data[datos][1]+"</h2>"+
                    "<img src='img/speaker.png' alt='listen'"+
                    'onclick="hablar(\''+data[datos][1]+'\',this,2)"'+
                    "class='opcionesSug rechazar listen'/></div>";
                }
            }
           }else{
               $("#palabraDefinicion").html(aux);
                $("#palabraPadreB").html(data[0][1]);
                $("#palabraPadreId").val(data[0][0]);
                $("#sTitulo").attr("onclick", "hablar('"+data[0][1]+"',this,1)");
                    con = "<div class='empty'>"+
              "<img src='img/clear.png' alt='empty' />"+
              "<h3>Sin Relaciones</h3></div>";
           }
            $("#resultadosBusqueda").html(con);
        }
      });
}

/**
* Function hablar
* Obtiene la palabra para poder usar la API responsiveVoice
* Se cambia el css de los iconos y reproduce la palabra
* @param  palabra
* @param  objeto
* @param tipo
*/
function hablar(palabra, objeto, tipo){
  if(tipo==1){
    objeto.style.opacity = "0.3";
  }else{
    objeto.style.background = "rgba(70, 126, 155, 0.3)";
  }
  palabra = palabra.toLowerCase();
  responsiveVoice.setDefaultVoice("Spanish Female");
  responsiveVoice.speak(palabra);
  setTimeout(function(){
    responsiveVoice.cancel();
    if(tipo==1){
      objeto.style.opacity = "1";
    }else{
        objeto.style.background = "transparent";
    }
   }, 2000);
}

/************Aprobar Sugerencias ****/
/**
* Function Sugerencias
* Manda un Ajax a sugerencuas/propuestas
* Obtiene los datos de las palabras y los inserta en un div
* el cual se agrega al contenedor donde se mostraran los resultados
* Si no retorna informacion mostrara una imagen de sin sugerencias
*/
$(document).ready(function(){
    if($("#sugerencias").length){
        usuario = localStorage.getItem('id_usuario')
      $.ajax({
        url: 'Backend/obtenerPropuestas.php',
        type:'GET',
          data: {usuario:usuario},
        success: function(data){
            data = JSON.parse(data);
          var con = "";
          if(data.length > 0){
            for(palabras in data){
                con += "<div  onclick='cambiar("+data[palabras][0]+")' class=' col-sm-5 col-md-5 col-lg-4 respuesta sugeridas'>"+
                "<div class='banderaP'><img src='img/banderas/"+data[palabras][2]+"' alt='bandera' class='banderaEspe'/></div>"+
                  "<div class='text-left pais2'><h1>"+data[palabras][1]+"</h1></div>"+
                  "<div class='text-center aceptar'><img src='img/next.png' alt='aceptar' onclick='cambiar("+data[palabras][0]+")'></div></div>";
            }
				 
          }else{
            con = "<div class='text-center empty'>"+
              "<img src='img/empty.png' alt='empty' />"+
              "<h3>Sin Sugerencias</h3></div>";
          }
          $("#respuestas").html(con);
        }
      });
    }
});
/**
* Function cambiar
* @param id
* Redirige a la ruta sugerencias/especifica
* con el id de la palabra seleccionada
*/
function cambiar(id){
    sessionStorage.setItem("idPalabra",id);
  window.location = "especificSugerencia.html";
}
/**
* Function obtenerEspecificos
* Obtiene las relaciones sugeridas con dicha palabra seleccionada
* Inserta los datos retornados en divs los cuales se agregan al contenedor
* contenedorSugerencias para mostrar los resultados
*/
$(document).ready(function(){
  if($("#palabraPadre").length){
      self.palabra("Cargando...");
    var id = sessionStorage.getItem("idPalabra");
    if(id != ""){
        usuario = localStorage.getItem('id_usuario');
      $.ajax({
        url: 'Backend/obtenerPropuestasEspecifico.php',
        type:'GET',
        data: {id:id, usuario:usuario},
        success: function(data){
            data = JSON.parse(data);
          var con = "";
          for(datos in data){
              if(datos == 0){
                   $("#palabraDefinicion").html(data[datos][4]);
                    $("#palabraPadre").html(data[datos][1]);
                    $("#palabraPadreId").val(data[datos][0]);
                }else{
                    con = con + "<div class='sugerenciaTarjeta' id='"+data[datos][5]+"'>"+
                    "<img src='img/banderas/"+data[datos][2]+"' alt='bandera' class='banderaSug'/>"+
                    "<h2>"+data[datos][1]+"</h2>"+
                    "<img src='img/aceptar.png' alt='aceptar' onclick='aceptar("+data[datos][5]+")' class='opcionesSug aceptar'/>"+
                    "<img src='img/cancel.png' alt='rechazar' onclick='rechazar("+data[datos][5]+")' class='opcionesSug rechazar'/></div>";
                }
          }
          $("#contenedorSugerencias").html(con);
        }
      });
    }
  }
});
/**
* Function aceptar
* Si se acepta la relación se manda un ajax a la ruta /aceptarPropuestas
* Si retorna un correcto se elimina el div del contenedorSugerencias
* Si el div queda vacio se retorna a /sugerencias
* @param id
*/
function aceptar(id){
    usuario = localStorage.getItem('id_usuario');
  $.ajax({
    url: 'Backend/aceptarPropuesta.php',
    type:'GET',
    data: {id:id, usuario:usuario},
    success: function(data){
      if(data == "correcto"){
        $("#"+id).remove();
        if($("#contenedorSugerencias").is(':empty')){
          window.location = "aceptarSugerencia.html";
        }
      }
    }
  });
}
/**
* Function rechazar
* Si se rechaza la relación se manda un ajax a la ruta /rechazarPropuestas
* Si retorna un correcto se elimina el div del contenedorSugerencias
* Si el div queda vacio se retorna a /sugerencias
* @param id
*/
function rechazar(id){
    usuario = localStorage.getItem('id_usuario');
  $.ajax({
    url: 'Backend/rechazarPropuesta.php',
    type:'GET',
    data: {id:id, usuario:usuario},
    success: function(data){
      if(data == "correcto"){
        $("#"+id).remove();
        if($("#contenedorSugerencias").is(':empty')){
          window.location = "aceptarSugerencia.html";
        }
      }
    }
  });
}
/**
* Function retrocederEsp
* Si se encuentra en la pagina de especificSugerencia retrocede
* a la pagina donde se encuentran las demas sugerencias
* solamente Redirige a la ruta /sugerencias
*/
function retrocederEspS(){
  window.location = "aceptarSugerencia.html";
}
//______________________________Seccion Reevaluacion

/**
* Function reevaluacion
* Obtiene las relaciones marcadas como rechazadas
* Las muestra en la pantalla
* Si no existen rechazadas muestra una imagen
*/
$(document).ready(function(){
    if($("#reevaluacion").length){
        usuario = localStorage.getItem('id_usuario');
      $.ajax({
        url: 'Backend/reevaluacionPropuestas.php',
        type:'GET',
          data:{usuario:usuario},
        success: function(data){
            data = JSON.parse(data);
          var con = "";
          if(data.length > 0){
            for(palabras in data){
                con = con + "<div onclick='cambiarRee("+data[palabras][0]+")' class=' col-sm-5 col-md-5 col-lg-4 respuesta sugeridas'>"+
                 "<img src='img/banderas/"+data[palabras][2]+"' alt='bandera' class='banderaEspe'/>"+
                   "<div class='pais2'><h1>"+data[palabras][1]+"</h1></div>"+
                  "<div class='aceptar'><img src='img/next.png' alt='aceptar' onclick='cambiarRee("+data[palabras][0]+")'></div></div>";
            }
          }else{
            con = "<div class='text-center empty'>"+
              "<img src='img/empty.png' alt='empty' />"+
              "<h3>Nada por Reevaluar</h3></div>";
          }
          $("#respuestas").html(con);
        }
      });
    }
});
/**
* Function cambiarRee
* @param id
* Redirige a la ruta reevaluacion/especifica
* con el id de la palabra seleccionada
*/
function cambiarRee(id){
    sessionStorage.setItem("idPalabra",id);
    window.location = "especificReevaluacion.html";
}
/**
* Function palabras
* Obtiene el detalle de la relación que sea rechazada
*/
$(document).ready(function(){
  if($("#palabraPadre2").length){
      self.palabra("Cargando...");
      usuario = localStorage.getItem('id_usuario');
    var id = sessionStorage.getItem("idPalabra");
    if(id != ""){
      $.ajax({
        url: 'Backend/obtenerPropuestasEspecificoRe.php',
        type:'GET',
        data: {id:id, usuario:usuario},
        success: function(data){
            data = JSON.parse(data);
          var con = "";
          for(datos in data){
              if(datos == 0){
                   $("#palabraDefinicion").html(data[datos][4]);
                    $("#palabraPadre2").html(data[datos][1]);
                    $("#palabraPadreId").val(data[datos][0]);
                }else{
                    con = con + "<div class='sugerenciaTarjeta' id='"+data[datos][5]+"'>"+
                    "<img src='img/banderas/"+data[datos][2]+"' alt='bandera' class='banderaSug'/>"+
                    "<h2>"+data[datos][1]+"</h2>"+
                    "<img src='img/aceptar.png' alt='aceptar' onclick='aceptarRe("+data[datos][5]+")' class='opcionesSug aceptar'/>"+
                    "<img src='img/cancel.png' alt='rechazar' onclick='rechazarRe("+data[datos][5]+")' class='opcionesSug rechazar'/></div>";
                }
          }
          $("#contenedorSugerencias").html(con);
        }
      });
    }
  }
});
/**
* Function aceptar
* Si se acepta la relación se manda un ajax a la ruta /aceptarPropuestas
* Si retorna un correcto se elimina el div del contenedorSugerencias
* Si el div queda vacio se retorna a /reevaluacion
* @param id
*/
function aceptarRe(id){
  var idE = "#"+id;
  usuario = localStorage.getItem('id_usuario');
  $.ajax({
    url: 'Backend/aceptarPropuestaRe.php',
    type:'GET',
    data: {id:id, usuario:usuario},
    success: function(data){
      if(data == "correcto"){
        $(idE).remove();
        if($("#contenedorSugerencias").is(':empty')){
          window.location = "aceptarReevaluacion.html";
        }
      }
    }
  });
}

/**
* Function rechazar
* Si se rechaza la relación se manda un ajax a la ruta /rechazarPropuestas
* Si retorna un correcto se elimina el div del contenedorSugerencias
* Si el div queda vacio se retorna a /reevaluacion
* @param id
*/
function rechazarRe(id){
  var idE = "#"+id;
    usuario = localStorage.getItem('id_usuario');
  $.ajax({
    url: 'Backend/rechazarPropuestaRe.php',
    type:'GET',
    data: {id:id, usuario:usuario},
    success: function(data){
      if(data == "correcto"){
        $(idE).remove();
        if($("#contenedorSugerencias").is(':empty')){
          window.location = "aceptarReevaluacion.html";
        }
      }
    }
  });
}
/**
* Function retrocederEsp
* Redirige a la ruta /reevaluacion
* Para mostrar las demas rechazadas
*/
function retrocederEsp(){
  window.location = "aceptarReevaluacion.html";
}
/********Reportar Palabras***/
/**
* Function ocultarReporte
* Oculta el div del reporte
*/
function ocultarReporte(){
  $("#reporte").css('visibility','hidden');
  $("#reporte").css('opacity','0');
  if($("#definicion").css('visibility')=='visible'){
    $("#definicion").css('visibility','hidden');
  }
  $("#reportein").css('visibility','hidden');
}

/**
* Mostrar reportar
* Muestra el div para reportar relacion
* cuando se presiona el div sobre 1 seg
*/
$(document).ready(function(){
  $("#resultadosBusqueda").hammer({domEvents:true}).on("press", "div", function() {
    $("#reporte").css('visibility','visible');
    $("#reporte").css('opacity','1');
    $("#reportein").css('visibility','visible');
    $("#idDin").val(this.id);
  });
});

/**
* Function reportar
* Envia el id de la palabra a reportar a la ruta index/reportarPalabra
* Si reotrna un correcto oculta las opciones de reportar y muestra un mensaje
* de palabra reportada
*/
function reportar(){
  var id = $("#idDin").val();
  usuario = localStorage.getItem('id_usuario');
  $.ajax({
    url: 'Backend/reportarPalabra.php',
    type:'GET',
    data: {id:id, usuario:usuario},
    success: function(data){
      if(data == "correcto"){
        self.ocultarReporte();
        self.palabra("Palabra Reportada");
      }
    }
  });
}
